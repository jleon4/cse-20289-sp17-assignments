Homework 02
===========
Abraham Leon

1a) if [ $# -eq 0 ]; then
    echo "Usage: extract.sh archive1 archive2..."
    exit 1
    fi

    Takes care of not having any arguments following the command. $# represents the number of arguments. If the number of arguments = 0 , then it will display a message and return an exit code of 1.

1b) In order to find which command to use for each of these, I had to google the file ending and look at the flags necessary.

1c) The most challenging aspect was figuring out the flags required for each file type. I overcame this issue by looking up each individual file type on google and looking for the flags necessary to unzip the file.  

Activity 2:

1) shuf <<EOF | head -n 1 | $COWS

This command places all of the sayings into a here document. It then shuffles the document and chooses a single line using head -n 1. This is pipelined into the $COWS variable which displays it using the cowsay program.

2) In order to handle signals, I used a function called cleanup which traps the 1 2 and 15 signals, which prevents the user from exiting the program using some common exit signals.

3) To read input from the user, I used the read command.

4) The most challenging aspect was figuring out how to use the shuf command and the here documents. I overcame this by asking a TA the correct syntax.

Activity 3: Meeting with Oracle

1. My first step was to scan `xavier.h4x0r.space` for a HTTP port:

        $  nmap -Pn xavier.h4x0r.space
        Nmap scan report for xavier.h4x0r.space (129.74.160.130)
          Host is up (0.40s latency).
          Not shown: 946 filtered ports, 50 closed ports
          PORT     STATE SERVICE
          22/tcp   open  ssh
          8888/tcp open  sun-answerbook
          9111/tcp open  DragonIDSConsole
          9876/tcp open  sd

          Nmap done: 1 IP address (1 host up) scanned in 65.43 seconds

    As can be seen, there are `2` ports in the `9000` - `9999` range.


2. Next, I tried to access the HTTP server:

        command: curl xavier.h4x0r.space:9876
        _____________________________________
/ Halt! Who goes there?                  \
|                                        |
| If you seek the ORACLE, you must       |
| _query_ the DOORMAN at                 |
| /{NETID}/{PASSCODE}!                   |
|                                        |
| To retrieve your PASSCODE you must     |
| first _find_ your LOCKBOX which is     |
| located somewhere in                   |
| ~pbui/pub/oracle/lockboxes.            |
|                                        |
| Once the LOCKBOX has been located, you |
| must use your hacking skills to        |
| _bruteforce_ the LOCKBOX until it      |
| reveals the passcode!                  |
|                                        |
\ Good luck!                             /
----------------------------------------

3. Next, I went to the location provided and saw many locked boxes.
 command: cd ~pbui/pub/oracle/lockboxes
 output: a bunch of random numbers and letters

4. Next I attempted to look for my specific lockbox:
command: find ~pbui/pub/oracle/lockboxes | grep jleon4
output:
/afs/nd.edu/user15/pbui/pub/oracle/lockboxes/cd7e8868/3597f66b/1d6f7d07/db4c70cb/jleon4.1
/afs/nd.edu/user15/pbui/pub/oracle/lockboxes/cd7e8868/3597f66b/1d6f7d07/db4c70cb/jleon4.lockbox
/afs/nd.edu/user15/pbui/pub/oracle/lockboxes/cd7e8868/3597f66b/1d6f7d07/db4c70cb/jleon4.2
 and many more...

 5. Next I attempted to brute force my lockbox
 command:
 strings /afs/nd.edu/user15/pbui/pub/oracle/lockboxes/cd7e8868/3597f66b/1d6f7d07/db4c70cb/jleon4.lockbox

 output:
 a bunch of random strings appear

 6. I attempted to open my lockbox by running it:
 command: /afs/nd.edu/user15/pbui/pub/oracle/lockboxes/cd7e8868/3597f66b/1d6f7d07/db4c70cb/jleon4.lockbox

 output:
 Usage: lockbox password

 I need to find a passworrd

7. Next I attempted to run through this list of strings and see which one the password is. To do this I used a for loop and tried every string.


for String in $(strings /afs/nd.edu/user15/pbui/pub/oracle/lockboxes/cd7e8868/3597f66b/1d6f7d07/db4c70cb/jleon4.lockbox)
do
  /afs/nd.edu/user15/pbui/pub/oracle/lockboxes/cd7e8868/3597f66b/1d6f7d07/db4c70cb/jleon4.lockbox $String
done


output:
94357a4a556a46ad240c2a655f4b5be2

8. Next I entered the oracle by ringing the DOORMAN
command:
curl xavier.h4x0r.space:9876/jleon4/94357a4a556a46ad240c2a655f4b5be2

output:
Ah yes, jleon4... I've been waiting for \
| you.                                    |
|                                         |
| The ORACLE looks forward to talking to  |
| you, but you must authenticate yourself |
| with our agent, BOBBIT, who will give   |
| you a message for the ORACLE.           |
|                                         |
| He can be found hidden in plain sight   |
| on Slack. Simply send him a direct      |
| message in the form "!verify netid      |
| passcode". Be sure to use your NETID    |
| and the PASSCODE you retrieved from the |
| LOCKBOX.                                |
|                                         |
| Once you have the message from BOBBIT,  |
| proceed to port 9111 and deliver the    |
| message to the ORACLE.                  |
|                                         |
| Hurry! The ORACLE is wise, but she is   |
\ not patient!                            /
-----------------------------------------


9. Next I attempted to talk to the ORACLE

the message from Slack was:
d3lyYmE0PTE0ODYwNzU5OTA=

command:
telnet xavier.h4x0r.space 9111

output:
< Hello, who may you be? >
 ------------------------
  \
   \          .
       ___   //
     {~._.~}//
      ( Y )K/  
     ()~*~()   
     (_)-(_)   
     Luke    
     Sywalker
     koala   
NAME? jleon4
 ___________________________________
/ Hmm... jleon4?                    \
|                                   |
| That name sounds familiar... what |
\ message do you have for me?       /
 -----------------------------------
  \
   \          .
       ___   //
     {~._.~}//
      ( Y )K/  
     ()~*~()   
     (_)-(_)   
     Luke    
     Sywalker
     koala   
MESSAGE? d3lyYmE0PTE0ODYwNzU5OTA=
 _______________________________________
/ Ah yes... jleon4!                     \
|                                       |
| You're smarter than I thought. I can  |
| see why the instructor likes you.     |
|                                       |
| You met BOBBIT about 4 minutes ago... |
\ What took you so long?                /
 ---------------------------------------
  \
   \          .
       ___   //
     {~._.~}//
      ( Y )K/  
     ()~*~()   
     (_)-(_)   
     Luke    
     Sywalker
     koala   
REASON? i suck
 ______________________________________
/ Hmm... Sorry, kid. You got the gift, \
| but it looks like you're waiting for |
| something.                           |
|                                      |
| Your next life, maybe. Who knows?    |
\ That's the way these things go.      /
 --------------------------------------
  \
   \          .
       ___   //
     {~._.~}//
      ( Y )K/  
     ()~*~()   
     (_)-(_)   
     Luke    
     Sywalker
     koala   

Congratulations jleon4! You have reached the end of this journey.

I hope you learned something from the ORACLE :]
