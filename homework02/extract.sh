#!/bin/sh

if [ $# -eq 0 ]; then
    echo "Usage: extract.sh archive1 archive2..."
    exit 1
fi

for File in $@
do
  case $File in
  	*.tgz | *.tar.gz  )
      tar zxvf $File
  		;;
  	*.tbz | *.tar.bz2 )
  	  tar xvjf $File
  		;;
  	*.txz | *.tar.xz )
      tar xvJf $File
      ;;
    *.zip | *.jar )
    	unzip $File
    	;;
    *)
      echo "Invalid Input"
      exit 1

  esac
done

exit 0
