#!/bin/sh

COWS=/afs/nd.edu/user15/pbui/pub/bin/cowsay

trap cleanup 1 2 15

cleanup()
{
  echo "Leaving so soon? I think not" | $COWS
}

#initial question

echo "How's it going, ask me anything" | $COWS

#while loop as long as user doesnt enter a questions with read command
read question
while [ -z "$question" ]; do

  echo "What's your question bud?"
  read question
done

shuf <<EOF | head -n 1 | $COWS
It is certain
It is decidedly so
Without a doubt
Yes, definitely
You may rely on it
As I see it, yes
Most likely
Outlook good
Yes
Signs point to yes
Reply hazy try again
Ask again later
Better not tell you now
Cannot predict now
Concentrate and ask again
Don't count on it
My reply is no
My sources say no
Outlook not so good
Very doubtful
EOF
