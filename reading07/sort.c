/* Abraham Leon
sort.c */

#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define MAX_NUMBERS (1<<10)

/* Functions */

size_t read_numbers(FILE *stream, int numbers[], size_t n) {
    size_t i = 0;

    while (i < n && scanf("%d", &numbers[i]) != EOF) {
        i++;
    }

    return i;
}

void sort_numbers(int numbers[], size_t n) {
  int i,j,temp;
  for(i=1;i<n;i++){
    temp = numbers[i];
    j=i-1;
    while(temp<numbers[j] && j>=0){
    numbers[j+1] = numbers[j];
    --j;
    }
  numbers[j+1]=temp;
  }
}

/* Main Execution */

int main(int argc, char *argv[]) {
    int numbers[MAX_NUMBERS];
    size_t nsize;

    nsize = read_numbers(stdin, numbers, MAX_NUMBERS);
    if (nsize == 0){
      printf("\n");
    }
    else {
      sort_numbers(numbers, nsize);

      int i;
      for(i=0;i<nsize-1;i++){
        printf("%d ", numbers[i]);
      }
      printf("%d\n", numbers[nsize-1]);
    }

    return EXIT_SUCCESS;
}
