Reading 07
==========

1. Describe what the read_numbers function does by explaining what is happening on the following line:

while (i < n && scanf("%d", &numbers[i]) != EOF)

n here is the size of the array. This while loop is gonna increase the value of i (which is what this function returns) only if the i is less than the inputted value of n and the file inputted through standard input has not reached an end. the scanf function takes input from standard in. It's first argument "%d" signifies that it will read an integer value, the & before numbers is crucial if using scanf. The & is used to get the address from a variable. The numbers array here is being modified until you run out of data from stdin or from the file steam. If this happens, you have reached EOF (end of file). Or, this loop will stop running if an error is encountered. Hence, during each iteration of this while loop, the variable i is being incremented every time there is a number from the file or from standard in. Hence, this function will return the total number of numbers.

2. Insteading of passing size_t n into the sum_numbers function, could we have used sizeof to get the size of the array as shown below?

When implementing the code given, the program still works. However, we should not use sizeof(array) if we want to get a number. For example:

int sum_numbers(int numbers[]) {
    int total = 0;
    size = sizeof(numbers)  
    for (size_t i = 0; i < size; i++) {
        total += numbers[i];
    }

    return total;
}

In C, array parameters are treated as pointers. This could cause problems with numbers going out of scope. Sizeof gives you how many bytes that data returns. Doing sizeof inside a function will not work. Doing size of outside a function would work.

1. Compare how this version of cat parses command line arguments versus the approach used in cat.py from Reading 05. Beyond the obvious differences in syntax, how is the code similar and how is it different?

Be sure to explain the role of argc and argv.

This version of cat is keeping a counter or index called argind in order to run through each argument following the command. In the python script, we "popped" each argument. During each iteration of the while loop each argument is returned as a string and popped or basically deleted and the next argument is the next element in the array. This is similar to shift in shell scripting. Within this cat.c , we are using a pointer to a string called arg in order to iterate through our arguments. Both versions use a condition that the length of the argument has to be greater than 1 and the first argument has to start with "-".

argc is the number of arguments following the command. argv[] can be used to invoke a specific argument following the command. It is the arguments vector.


2. Describe how each file is processed by explaining the following code:

while (argind < argc) {
    char (star)path = argv[argind++];
    if (strcmp(path, "-") == 0) {
        cat_stream(stdin);
    } else {
        cat_file(path);
    }
}

As long as the argument index has not exceeded the total argument count (i.e all of the arguments have been accounted for), path (a string pointer to an argument) is compared to "-".
In other words, char (star)path = argv[argind++] is pointing to the value of argv[] and increasing the index after setting its value. Hence, it will iterate through all of the arguments one at a time.
If this value of path is '-'(i.e, there is a '-' where the pointer currently is), this strcmp will return a 0, otherwise, it will return a number greater than or less than 0.

3. Explain how the cat_stream function works. What are the purposes of fgets and fputs?

In this function, the fgets() function reads a line from the specified stream and stores it into the string pointed to by buffer. Basically, this function stores the input into the character array called buffer for as long as there is input to store. The buffer size BUFSIZ will be the total number of characters stored in this array.
The fputs() function writes a string to the specified stream up to but not including the null character. Basically, this function places all of the content within the first parameter into the second parameter. In this specific program, the contents of buffer which includes everything that has been passed in from the standard in stream, in being sent to standard out.

4. Explain how the cat_file function works. What is the purpose of the following if statement:

This function takes in the path string pointer which points to the argument vector as a constant in order to prevent any modifications. The if statement is in case the file is empty or there is an error in the file. It will print out the program name as a string, in this case NULL, the passed it variable path, and strerror(errno) searches an internal array for the error number errnum and returns a pointer to an error message string. 
