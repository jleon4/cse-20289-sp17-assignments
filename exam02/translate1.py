#!/usr/bin/env python2.7
import re
import os

with open('/etc/passwd', 'r') as f:
    text = f.readlines()

for line in text:
    field = line.split(":")[4]
    regex = r'[uU]ser'
    result = set([x for x in field if re.search(regex,x)])

    for string in result:
        string.lower()

for x in sorted(result):
    print x
