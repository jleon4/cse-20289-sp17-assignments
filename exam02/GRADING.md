GRADING - Exam 02
=================

- Identification:   2.25
- Web Scraping:     2.75
- Generators:       4.75
- Concurrency:      5.25
- Translation:      5.75
- Total:	    20.75

Comments
--------

- 0.25  Translation Q1  - Need to store lowercase in list
