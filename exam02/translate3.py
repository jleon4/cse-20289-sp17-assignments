#!/usr/bin/env python2.7

import os

x = os.popen('ps aux', 'r')

lines = [str(line.split()[0]) for line in x.read().rstrip().split('\n')]

result = dict()
for x in sorted(lines):
    if x not in result.keys():
        result.update({x:1})
    else:
        result[x] = result[x]

print len(result)
