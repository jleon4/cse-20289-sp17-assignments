Homework 01 Grading
==================

1. +2

2. +3

3. +3

3.5) This is correct, however there is an even better answer that explains the efficiency difference. Using a pipeline requires creating a process (i.e. cat), opening the math.txt file, and then creating a pipe to feed the data to bc.  Using I/O redirection, the shell simply opens the math.txt file and then passes it to bc (no need for a process). No credit off. 

4. +4

5. +3

