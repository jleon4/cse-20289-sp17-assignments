Homework 01
===========
Abraham Leon

Activity 1:
  1a) On my home directory:
    Normal rights:
      nd_campus l  (campus has access to list the names and dates of files in this directory, but nothing else)
      system:administrators rlidwka (admins have all of the Access control lists as seen below)
      system:authuser l (has access to list the names and dates of files in this directory, but nothing else)
      jleon4 rlidwka (same ACLS as admins)

    on my Public directory:
    Normal rights:
      nd_campus rlk (campus has right to read, lookup and lock files in this directory)
      system:administrators rlidwka
      system:authuser rlk
      jleon4 rlidwka

    on my Private directory:
    Normal rights:
      system:administrators rlidwka  
      jleon4 rlidwka

      only system administrators and myself have any ACls within this directory.

    where:
    Read	R	read the contents of files in this directory.
    Lookup	L	list the names and dates of files in this directory, and the ACLs forthe directory itself.
    Insert	I	create new files in this directory.
    Delete	D	delete files from this directory.
    Write	W	modify the contents of files in this directory.
    Lock	K	lock files, temporarily preventing any one else from modifying thefile.
    Admin	A	modify the ACLs for this directory.

  1b) rlk given to nd_campus makes the public directory Public.
      the lack of any ACLs given to nd_campus and system:authuser makes the Private directory private.

  2a)cannot touch 'permission denied'

  2b)since I was denied permission to create a file, I must be missing the ACL i which allows me to create new files in this directory. It must be given only to the admins. Hence, the ACLs take precidence over Unix.

Activity 2:
  1a)
  | Command                                                                                       | Elapsed Time  |
  |-------------------------------------                                                          |---------------|
  | cp -r /usr/share/pixmaps /afs/nd.edu/user23/jleon4/images                                     | 1.74 seconds |
  | mv /afs/nd.edu/user23/jleon4/images /afs/nd.edu/user23/jleon4/pixmaps                         | 0.006 seconds     |
  | mv /afs/nd.edu/user23/jleon4/pixmaps /tmp/jleon4-pixmaps                                      | 0.954 seconds     |
  | rm /tmp/jleon4-pixmaps                                                                        | 0.002 seconds     |

  1b) when renaming, we are simply changing the name of the folder. No files are being transferred to different directories. On the other hand, when transferring the files, the CPU and RAM are used which takes significantly more time than simply changing the name of the directory.

  2) Moving it would require to find a new location, copy files and move them to this location as well as deleting the files in the old location. Removing it simply takes away the reference. You can no longer have access to this data. This is why removing takes significantly less time.

Activity 3:
  1) bc < math.txt
  2) bc < math.txt > results.txt
  3) bc < math.txt > results.txt 2> /dev/null
  4) cat math.txt | bc
  4b) Using cat pipeline will run through each line of code and more than once based on the number of commands and pipelines. On the other hand, a simple redirection will only run through each line once.

Activity 4:
  1) cat /etc/passwd | grep /sbin/nologin | wc -l
    A total of 48
  2) who | awk '{print $1}'| sort|uniq | wc -l
    18 unique users
  3) du /etc -h | sort -n | sort -h 2> /dev/null  
    7.0M	/etc/selinux/targeted/policy
    7.6M	/etc/selinux
    7.6M	/etc/selinux/targeted
    8.4M	/etc/gconf
    36M	/etc
  4) ps aux | grep bash | grep -v grep | wc -l

Activity 5:
  1a) Control + Z only stopped the process. It did not terminate.
    Control + C did not terminate the process.
    bg

  b) kill -9 3218
   3218 is the PID
   -9 was the signal to kill

  2a) ps u | grep -b TROLL | grep -v grep | awk '{print $2}' | xargs kill -9
  b) killall -u jleon4 -9 TROLL

  3) pgrep -u jleon4 TROLL | xargs kill -ILL
     pgrep -u jleon4 TROLL | xargs kill -ALRM
     pgrep -u jleon4 TROLL | xargs kill -HUP
     pgrep -u jleon4 TROLL | xargs kill -USR1
     pgrep -u jleon4 TROLL | xargs kill -USR2
