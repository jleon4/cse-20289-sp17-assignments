Reading 09 - Grading
====================

**Score**: 3/4

Deductions
----------
-.25 Q1.2: important difference is that union consists of the memory space for the largest member while structs consists of the memory space of all the members 
-.25 Q2.2: should be more detailed and discuss that characters represented be integers and multi-byte integers (longs, ints, shorts)
-.5 Q4.2: table is mostly wrong

Comments
--------
