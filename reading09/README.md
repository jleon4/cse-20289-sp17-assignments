Reading 09
==========

1. What is the size of s and what is the size of u?
size of s: 16 bytes
size of u: 8 bytes

2. What is the difference between a union and a struct?
A union is basically a struct except that instead of containing multiple variables each with their own memory a Union allows for multiple names to the same variable.

1. What message does this program output?
Fate is inexorible!


2. Use the DUMP_BITS macro to explore this program and then explain how it works (that is, how does setting the values above in the struct Thing t object yield the final message).

fate is inexorable!
 00100000 01110011 01101001 00100000 01100101 01110100 01100001 01100110

What does this program tell you about types, memory, and how data is represented in C? What are we really doing when we cast values in C?

In C, everything can be represented as binary. When we talk about casting values in C, we are simply allocating different memory sizes depending on the type of value that we want to cast. Here, we get 8 bytes of data, which is the number of bytes required for a "long" type.


1. What exactly is a collision?
This is when two different inputs give the same output in a hashing function.

2. How are collisions handled in a hash table that uses separate chaining?
All keys that map to the same hash value are kept in a list (or “bucket”).
Each bucket is independent, and has some sort of list of entries with the same index.

3. How are collisions handled in a hash table that uses open addressing (e.g. linear probing)?
All entry records are stored in the bucket array itself. The address of each value is not determined by the hash table (hence the term open addressing). If a collision happens, linear probing searches the table for the closest following free location and inserts the new key there.

4.
h(x) = x % 10
Bucket	Value
0	        NULL
1	        NULL
2	        2, 72
3	        3, 13
4	        14
5	        NULL
6	        56
7	        7
8	        78, 68
9         79

h(x) = x % 10
Bucket	Value
0	        7
1	        3
2	        2
3	        78->8
4	        56->6
5	        72->2
6	        79->9
7	        68->8
8	        13->3
9         14->4
