#!/bin/sh


if [ "$#" = "0" ] ; then
  DELIM="#"
  sed -e "s|$DELIM.*$||" | sed 's_\s*$__' | sed '/^$/d'
fi

if [ "$1" = "-h" ]; then
  echo "Usage: broify.sh

  -d DELIM    Use this as the comment delimiter.
  -W          Don't strip empty lines."
  exit 1
fi

if [ "$1" = "-W" ]; then
  DELIM="#"
  sed -e "s|$DELIM.*$||" | sed 's_\s*$__'
fi

if [ "$1" = "-d" ] && [ "$#" -eq "2" ]; then
  DELIM=$2
  sed -e "s|$DELIM.*$||" | sed 's_\s*$__' | sed '/^$/d'
fi

if [ "$1" = "-d" ] && [ "$3" = "-W" ]; then
  DELIM=$2
  sed -e "s|$DELIM.*$||" | sed 's_\s*$__'
fi
