Homework 03
===========

Activity 1:
  1. How you parsed the command line arguments.
    In order to parse the command line arguments, I used $argument to see if there was a single argument passed. It was assumed that this argument would be an integer. This first argument $1 would be used to determine the "key" for the encryption.

  2. How you constructed the source set.
    My source set in this program was the entire alphabet both lowercase and uppercase.
    I made 2 different variables with all of these values and made my source set just be a combination of these two. Hence, my source set was:
    $sourceL$sourceC

  3. How you constructed the target set.
    My target set consisted of 4 smaller subsets which all took care of one of two of the parts of alphabet. SET1 would cut values 1-key value, SET2 would get the other part. Similarly for the uppercases SET3 would get the first part of the alphabet and SET4 would take care of the rest. My target set was made from all of these sets. In order to make the encryption possible, both sets were flipped. Hence, the final target set was: $SET2$SET1$SET4$SET3

  4. How you used both of these sets to perform the encryption.
    In order to perform the encryption, the tr command was used. This command translates characters. Hence, we are translating the normal alphabet to the shifted target set.

Activity 2:
  1. How you parsed the command line arguments.
    In the terminal I passed in a txt file using a "<". I used the $arguments to decide what this shell would perform.

  2. To remove comments I used the sed command.
    sed -e "s|$DELIM.*$||"
    This removes any lines starting with the set DELIM variables

  3. To remove any empty lines, I also used the sed command followed by:
    -e '/^$/d'
    This deletes any lines starting with white space.

  4. How the command line options affected your operations above.
    The command line arguments decide what command to execute. In order to take care of this, I used several if statements.  

Activity 3:
  1. To parse the command line arguments, I used if statements. 4 conditions read the arguments passed in from the command line and decide which command to execute based on the flags given.

  2. To extract the zip codes I used the curl command followed by the website. Then I used a grep command grep -Eo '>[0-9]{5}<' | grep -Eo [0-9]{5} which would only return 5 numbers from 0-9 or the zip codes.

  3. How you filtered by STATE and CITY.
  To filter by state and city, I used flags within a case statements. Hence, a city would only be filtered if the -c flag was used. Further, I used grep -E "/$CITY/" in order to add the city to the command. state is done in a similar manner.

  4. How you handled the text and csv FORMAT options.
  text format was handled by simply using the grep commands
  However, for csv format, I used the tr command to translate all of the new lines to commas doing: tr '\n' ','    after, i used a sed command to make add a new line after the very last zip code.  sed 's/.$//'  

  
