#!/bin/sh

# default variables
FORMAT=text
CITY=NA
STATE=Indiana

while [ $# -gt 0 ]
do
  case $1 in
  	-h)
    echo "Usage: zipcode.sh

    -c      CITY    Which city to search
    -f      FORMAT  Which format (text, csv)
    -s      STATE   Which state to search (Indiana)

    If not CITY is specified, then all the zip codes for the STATE are displayed."
    exit 1
  		;;
    -f)
      FORMAT=$2
      shift
      ;;
  	-c)
      CITY=$2
      shift
  		;;

    -s)
      STATE=$(echo "$2" | sed 's/ /%20/') #gets rid of spaces in state name
      shift
    	;;
  esac
  shift
done

if [ "$CITY" == NA ] && [ $FORMAT == text ]; then
  curl -sL http://www.zipcodestogo.com/$STATE/ |  grep -Eo '>[0-9]{5}<' | grep -Eo [0-9]{5}
elif [ "$CITY" != NA ] && [ $FORMAT == text ]; then
  curl -sL http://www.zipcodestogo.com/$STATE/ | grep -E "/$CITY/"| grep -Eo '>[0-9]{5}<' | grep -Eo [0-9]{5}
elif [ "$CITY" != NA ] && [ $FORMAT == csv ]; then
  curl -sL http://www.zipcodestogo.com/$STATE/ | grep -E "/$CITY/" | grep -Eo '>[0-9]{5}<' | grep -Eo [0-9]{5}  | tr '\n' ',' | sed 's/.$//'
elif [ "$CITY" == NA ] && [ $FORMAT == csv ]; then
  curl -sL http://www.zipcodestogo.com/$STATE/ | grep -Eo '>[0-9]{5}<' | grep -Eo [0-9]{5} | tr '\n' ',' | sed 's/.$/\n/'
fi

exit 0

#grep -Eo [0-9]{5}
#| sort | uniq
