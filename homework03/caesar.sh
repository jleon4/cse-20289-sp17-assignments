#!/bin/sh

sourceL=abcdefghijklmnopqrstuvwxyz
sourceC=ABCDEFGHIJKLMNOPQRSTUVWXYZ

if [ "$1" = "-h" ]; then
  echo "Usage: caesar.sh [rotation]"
  exit 1
fi

if [ "$#" -eq "1" ]; then
  key=$(($1%26))
fi

if [ "$#" -eq "0" ]; then
  key=13
fi

# takes care of the encryption
SET1=$(echo $sourceL | cut -c 1-$key)
SET2=$(echo $sourceL | cut -c $(($key+1))-26)
SET3=$(echo $sourceC | cut -c 1-$key)
SET4=$(echo $sourceC | cut -c $(($key+1))-26)

tr $sourceL$sourceC $SET2$SET1$SET4$SET3
