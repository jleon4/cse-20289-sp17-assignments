Homework 03 - GRADING 
===========================================
**Score**: 14.75/15

**Grader**: Mimi Chen

Deductions 
==========================

Activity 1
--------------------
**Score**: 5/5

**caesar.sh code**

Good job!

**test_caesar.sh**

Passes all tests


Activity 2
---------------------
**Score**: 5/5

**broify.sh code**

(-0) Does not call the help usage function

(-0) Code is very repetitive. You should use a while loop to process the command line arguments. Additionally, instead of using all the if statements, you can have a global variable DELIM = '#' which gets updated if there is a flag '-d'


**test_broify.sh**

Passes all tests



Activity 3
----------------------
**Score**: 4.75/5

**zipcode.sh code**

(-0.25) Need to verify if format is "text" or "csv". If neither, you should call the usage function and exit.

(-0) Code is very repetitive with 4 if statements. You can create a function "format" which determines whether or not the format should be "text" or "csv", which would reduce the 4 if statements to 2

**test_zipcode.sh**

Passes all tests


Comments
=====================
Good job!
