Homework 06 - Grading
=====================

**Score**: 14/15

Deductions
----------
-.25 Makefile: a couple rules have incorrect dependencies
-.25 readme Act1 Q1: should have found solution with time complexity O(n)
-.25 readme Act1 Q2: should have found solution with time complexity O(n)
-.25 readme Act2 Q2: translate-dynamic question should mention adding currently directory to LD_LIBRARY_PATH

Comments
--------
nice job
