/* stringutils.c: String Utilities */
// scp ./* jleon4@student00.cse.nd.edu:~/SP/homework06/
#include "stringutils.h"

#include <ctype.h>
#include <string.h>

char *	string_lowercase(char *s) {
  for (char * c = s; *c!='\0'; c++){
    *c = tolower(*c);
  }
    return s;
}


char *	string_uppercase(char *s) {
  for (char * c = s; *c!='\0'; c++){
    *c = toupper(*c);
  }
    return s;
}


bool	string_startswith(char *s, char *t) {
  int slen = strlen(s);
  int tlen = strlen(t);
  bool condition = true;
  if (tlen > slen){ condition = false;}
  for (char *x = t; *x!='\0'; x++, s++){
    if (*x != *s){
      condition = false;
    }
  }
  return condition;
}


bool	string_endswith(char *s, char *t) {
  int slen = strlen(s);
  int tlen = strlen(t);
  s += slen - tlen;
  bool condition = true;
  if (tlen > slen){ condition = false;}
  for (char *x = t; *x!='\0'; x++, s++){
    if (*x != *s){
      condition = false;
    }
  }
  return condition;
    return true;
}


char *	string_chomp(char *s) {
  char *c = s;
  int slen = strlen(s);
  c += slen - 1;
  if (*c == '\n'){
    *c = '\0';
  }
    return s;
}


char *	string_strip(char *s) {
  char *r = s;
    char *w = s;
    if (strlen(s) == 0) return s;
    while (*r == ' ') {
        r++;
    }
    while (*r != '\0') {
        *w = *r;
        w++;
        r++;
    }
    *w = *r;
    r = s + strlen(s) - 1; 
    while (*r == ' ') {
        r--;
        *(r+1) = '\0';
    }
    return s;
  // size_t size;
  // char *end;
  // size = strlen(s);
  // if (!size){return s;}
  //
  // end = s + size - 1;
  // while (end >= s && isspace(*end)){end--;}
  // *(end + 1) = '\0';
  // while (*s && isspace(*s)){s++;}
  //
  // return s;
}


static char *	string_reverse_range(char *from, char *to) {
  for (char *k = from; k < to; k++, to--) {
    char temporaryInput = *to;
    *to = *k;
    *k = temporaryInput;
  }
  return from;
}

/**
 * Reverses a string.
 * @param   s       String to reverse
 * @return          Pointer to beginning of modified string
 **/
char *	string_reverse(char *s) {
  int size = strlen(s);
  if (size == 0){return s;}

  char *c = s + strlen(s)-1;
  string_reverse_range(s,c);
    return s;
}


char *	string_reverse_words(char *s) {
  int size = strlen(s);
  if (size == 0){return s;}
  string_reverse_range(s, s + strlen(s) - 1);
  char* c = s, *t = s;
  while (*t) {
    while (*t && isspace(*t) == false) {
      t++;
    }
    string_reverse_range(c, t - 1);
    c = t;
    while (*t && isspace(*t)) {
      t++;
      c++;
    }
  }
  return s;
}


char *  string_translate(char *s, char *from, char *to) {
  if (strlen(s) == 0 || strlen(from) == 0 || strlen(to) == 0) {return s;}
  //char *to_ptr = to;
  for (char *c = s; *c != '\0'; c++) {
    for (char *d = from, *to_ptr = to; *d != '\0'; d++, to_ptr++) {
      if (*c == *d) { //swap characters
        *c = *to_ptr;
      }
    }
  }
    return s;
}
/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int	string_to_integer(char *s, int base) {
  int mult = 1;
  int final = 0;
  int size = strlen(s);
  char*c = s + size-1; //points to end of string

  while (*c){
    if (isalpha(*c)){
      if (*c >= 'a' && *c <= 'f'){
        final = final+(mult*(*c-- - 'a'+10)); //decrements
      }
      else if (*c >= 'A' && *c <= 'F'){
        final = final+(mult*(*c-- - 'A'+10)); //decrements
      }
    }
    else{
      final = final+(mult*(*c-- - '0'));
    }
    mult = mult*base;
  }
    return final;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
