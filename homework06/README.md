Homework 06
===========

1. Describe how your string_reverse_words function works and analyze its time complexity and space complexity in terms of Big-O notation.

string_reverse_words works by using the string_reverse_range function within. It first reverses the entire string from the beginning of the string (s) until the end. Next, two pointers with the value of s are instantiated (t and c). These will help us keep track of where a word ends. Running through the string, if the value of the pointer is not a space, the pointer r will increment. the value of c will be set to the value of t. if the value of pointer t is a space, both pointers c and t will increment. On the next iteration of the loop the string reverse range function will be called from the c(which is pointing to the beginning of the word) to t-1 (which points to the end). Hence, words are reversed.

The time complexity is Big O n^2. A loop inside a loop. In terms of space it is O 1, it is constant space.

2. Describe how your string_translate function works and analyze its time complexity and space complexity in terms of Big-O notation.

String translate runs through the string using two for loops. It uses two pointers to compare the values of the string. If the values of the pointers match what character we want to change from to "to", it will swap the two characters. Hence, translating a string.

The time complexity is Big O n^2. A loop inside a loop. In terms of space it is O 1, it is constant space.

3. Describe how your string_to_integer function works and analyze its time complexity and space complexity in terms of Big-O notation.

string to integer takes in a base, we initialize a mult variable which will be used to multiply to the value of the pointer. we instantiate a pointer to the end of the string called c. We start from the back of the string and see if the value of our pointer is either a number or a character. If it is an alphabetical letter in hex( we check for lowercase and upper case), the final value which we want retuned is the multiplier times the value of the pointer (converted to integer by subtracting the ascii value 'a' or 'A' plus 10). If the value is a number, we do the same but subtract the ascii value of '0' to turn into an integer. the pointer decrements each time. Hence we are going from the back to the front. The multiplier is multiplied by the base in each iteration of the loop. the final value is returned in integer.
The time complexity is Big O n. A loop. In terms of space it is O 1, it is constant space.


4. What is the difference between a shared library such as libstringutils.so and a static library such as libstringutils.a?

By default, GCC will produce dynamic executables that require the presence of libraries at run-time. Hence, we need shared libraries which can be linked to other applications. On the other hand, for static executables, we require static libraries, which are created differently. These static executables dont depend on any shared libraries. With a shared library, we have to run through the entire library every time we run an executable. However, for a static library, we do not.


Comparing the sizes of libstringutils.a and libstringutils.so, which one is larger? Why?


-rw-r--r--  1 jleon4 dip  12K Mar 24 12:50 libstringutils.a
-rwxr-xr-x  1 jleon4 dip  14K Mar 24 12:50 libstringutils.so

libstringutils.so is a larger file.

Activity 2

1. Describe how the translate.c program works. In particular, explain:

How you implemented command line parsing.
  I iterated through the argv[] using argind which was an index that increased during every iteration of the while loop. A case statement took care of each flag.

How you implemented the translation.
  I passed in the stream using a while loop as well as the fgets function. It stored the stream in a char array called buffer and applied the desired functions to this stream using by calling the appropriate functions. This implemented the translation.

How you implemented the different post processing filters.
  To do this, i simply called the functions that I created in the stringutils.c. I used if statements to implement the different filters.

Be sure to describe how you used and handled the mode bitmask.
  To do this, I used an enumerator which assigned a bit value to each function. In the case statements when parsing the command line arguments, the mode was assigned to each function. Then this mode was used within the translate_stream function.


What is the difference between a static executable such as translate-static and a dynamic executable such as translate-dynamic?
static executables require static libraries. dynamic executables require shared libraries.

Comparing the sizes of translate-static and translate-dynamic, which one is larger? Why?
-rwxr-xr-x  1 jleon4 dip  14K Mar 24 12:50 translate-dynamic
-rw-r--r--  1 jleon4 dip  11K Mar 24 12:50 translate.o
-rwxr-xr-x  1 jleon4 dip 885K Mar 24 12:50 translate-static

static requires a static library which is stored entirely within. It is portable. dynamic requires the executable to search through libraries every time it is called. Hence it is not actually stored within, taking up less memory.

When trying to execute translate-static, does it work? If not, explain what you had to do in order for it to actually run.

echo "   Hello World" | ./translate-static -l

It works.

When trying to execute translate-dynamic, does it work? If not, explain what you had to do in order for it to actually run.

./translate-dynamic: error while loading shared libraries: libstringutils.so: cannot open shared object file: No such file or directory

In order to run, I need to search through the shared libraries. 
