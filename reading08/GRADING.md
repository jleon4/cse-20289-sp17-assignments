Reading 08 - Grading
====================

**Score**: 2.75/4

Deductions
----------
-.25 Q1.3: 13 bytes - pointer is 8 bytes + 5 bytes of chars
-.25 Q1.5: 8 bytes - pointer is 8 bytes
-.25 Q1.6: 24 bytes  - pointer is 8 bytes + 1 point struct of 16 bytes
-.25 Q1.7: 168 bytes - pointer is 8 bytes + 10 point structs
-.25 Q1.8: 88 bytes - pointer is 8 bytes + 10 pointers to structs

Comments
--------
