/* duplicates.c */
//scp ./* jleon4@student00.cse.nd.edu:~/SP/reading08/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const int NITEMS = 1<<10;
const int TRIALS = 100;

bool contains(int *a, int n, int k) {
  for (int i = 0; i < n; i++){
    if (a[i] == k){return true;}
  }
    return false;
}

bool duplicates(int n) {
    int *randoms = malloc(n*(sizeof(int)));

    for (int i = 0; i < n; i++)
    	randoms[i] = rand() % 1000;

    for (int i = 0; i < n; i++) {
        /* TODO: Use contains to search array for duplicate */
    	if (contains(randoms,n,randoms[i+1])) {
          free(randoms);
    	    return true;
        }
    }
    free(randoms);
    return false;
}

int main(int argc, char *argv[]) {
    srand(time(NULL));

    // int array[] = {5,6,7,8,9};
    // if (contains(array, 5, 8)){printf("This is successful");}

    for (int i = 0; i < TRIALS; i++)
    	if (duplicates(NITEMS))
	    puts("Duplicates detected!");

    return 0;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
