Reading 08
==========

1. How much memory (in terms of bytes) is allocated or reserved for each of the 8 declarations above (assume a 64 bit Linux machine such as the student machines)?

1. 4 bytes
2. 20 bytes
3. 5 bytes
4. 16 bytes
5. 16 bytes
6. 16 bytes
7. 160 bytes
8. 80 bytes

1. Describe what the memory error was in Task 3 and how you fixed it.

The memory error was:
"Invalid write of size 4"

I was attempting to access memory that had yet to be allocated. When allocating memory dynamically, the function malloc requires the right amount of bits. Hence we cannot just pass the argument n. To fix this issue, I did malloc(n*(sizeof(int))); This in turn, will return 4(the number of bytes an int takes up) times n which is the number of items that we have assigned. I am now accounting for all the bytes required.


2. Describe what the memory leak was in Task 4 and how you fixed it.

error: 409,600 bytes in 100 blocks are definitely lost in loss record 1 of 1.

This issue was caused by the lack of the free() function within the if statement. If there was a duplicate, I would not free up the memory. Hence there was a memory leak. I added free(randoms) within the if statement in order to fix this memory leak.
