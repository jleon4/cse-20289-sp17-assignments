Homework 05
===========

Abraham Leon

Describe how you implemented the hulk.py script. In particular, briefly discuss:

How you generated all the candidate password combinations:
In order to do this, I made a function which created all of the permutations for a given alphabet and a given length. This in theory would include all of the candidate password combinations. Within my smash function, I saved all these permutations and added the specified prefix (if any) to each.

How you filtered the candidates to only contain valid passwords.
To filter the possible candidates, I used the following list comprehension.
final = [i for i in lst if md5sum(i) in hashes]
This will only return the candidates that are within my specified hashes path. if the md5sum of the specified permutation is in hashes, it will be passed the variable final.

How you handled processing on multiple cores.
To handle processing of multiple cores, I used a subsmash to allow for parallel execution. This basically divides the work normally done by one process. The subsmash only takes the prefix argument. subsmash, a pool variable created using the number of cores chosen by the user (created using multiprocessing.Pool), along with itertools.chain.from_iterable creates a list of passwords.

How you verified that your code works properly.
To verify my code, I ran tested each function one by one. I also took advantage of doctests provided to us by the instructor. After verifying that the functions passed the tests, I tested my code with the examples provided on the assignment page. They each matched the correct output. Hence, I verified that my code worked as it should.

Complete the following table for passwords of length 6:

16:
  real	19m16.894s

8:
  real	25m23.712s

4:
  real	47m56.656s

2:
  real	69m37.064s

1:
  real	92m23.152s

From your experience in this project and recalling your Discrete Math knowledge, what would make a password more difficult to brute-force: more complex alphabet or longer password? Explain.

A longer password would make a password much more difficult to brute-force. This exponentially increases the possible combinations. This would take significantly more time and processing power.

Activity 2:
How does the iv_map.py script work.
How does it keep track of line numbers.

To keep track of lines, this program has a counter variable called count which increases after the program runs through each line of the sampled text. All of the words contained in the line are appended into within this list. The .format takes care of displaying the correct line number next to each key in the list.

How does it remove undesirable characters.

to remove undesirable characters, a variable alphabet was created which contains all of the characters that we desire. When appending the words to the list, we use the .join function to join strings to the list. This will make every letter in each word lowercase as long as the character/ letter is within our alphabet variable.

How does the iv_reduce.py script work.
How does it aggregate the results for each word.

To aggregate the results of each word, the program adds a set where the value of the key used to be. Hence, the dictionary counts, has a key (the string or word) and a set. This set eliminates any duplicates of line numbers as well. To add values to this set, the .add function was used.

How does it output the results in the correct format.
To format the results, a similar process to the map program was used. A for loop loop for both key and value in sorted items in dictionary places all of the words in alphabetical order. Will print out a word one line at a time. Using a ./format spaced by a tab, it will print out the word (k) and will sort the number values using a for loop and the sorted function as long as the number is not = to ' '. This prints out every word in alphabetical order, and the line number for which each word appears in.
