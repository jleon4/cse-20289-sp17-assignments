#!/usr/bin/env python2.7
# Abraham Leon
# map.py
import sys
import string

count = 0
alphabet = 'abcdefghijklmnopqrstuvwxyz-'
lst = [] #creates a list

for line in sys.stdin:
    count = count + 1
    for word in line.strip().split():
        lst.append('{}\t{}'.format(''.join([make.lower() for make in word if make.lower() in alphabet]), count))
#print
for i in lst:
    print i
