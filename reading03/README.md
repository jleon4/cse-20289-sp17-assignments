Reading 03
==========

1. Convert all the input text to upper case:
tr '[:lower:]' '[:upper:]'

2. Find and replace all instances of monkeys to gorillaz:
sed 's/monkeys/gorillaz'

3. To remove all whitespace (including tabs) from left to first word, enter:
sed -e 's/^[ \t]*//'

4. Parse the /etc/passwd file for the shell of the root user:
grep '^root' | cut -d ":" -f 7

5. Find and replace all instances of /bin/bash, /bin/csh, and /bin/tcsh to /usr/bin/python in /etc/passwd:
sed -r -e 's_/bin/bash|/bin/csh|/bin/tcsh_/usr/bin/python/g'

6. Find all the records in /etc/passwd that have a number that begins with a 4 and ends with a 7:
grep -E '4[0-9]+7|47'

7. Given two text files, show all the lines that are present in both files.
comm -3 FILE1 FILE2

8. Given two text files, show which lines are different.
diff -a -y FILE1 FILE2
