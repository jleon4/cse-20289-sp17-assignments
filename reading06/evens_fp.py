#!/usr/bin/env python2.7

import sys


print ' '.join(filter(lambda number: int(number) % 2 == 0, map(lambda number: number.strip(), sys.stdin)))
