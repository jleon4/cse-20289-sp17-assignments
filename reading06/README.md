Reading 06
==========

A. What problem is MapReduce trying to solve?

MapReduce is trying to simplify large scale computations. It basically solves problems that are huge but not hard. If a problem can be solved efficiently using distributed computing, it is a candidate for MapReduce.
MapReduce provides:
Automatic parallelization and distribution
Fault-tolerance
I/O scheduling
Status and monitoring

B. Describe the three phases of a typical MapReduce workflow.

Map stage: In this stage, the input file is passed to the mapper line by line. The mapper processes the data and creates several small chunks of data.

Shuffle stage: This phase consumes output of Mapping phase. Its task is to consolidate the relevant records from Mapping phase output.

Reduce stage: output values from Shuffling phase are aggregated. This phase combines values from Shuffling phase and returns a single output value. In short, this phase summarizes the complete dataset.
