#!/usr/bin/env python2.7

import sys

def evens(stream):
    for number in sys.stdin:
        number = number.strip()
        if int(number) % 2 == 0:
            yield (number)


print ' '.join(evens(sys.stdin))
