#!/usr/bin/env python2.7
#Abraham Leon
#scp ./reddit.py jleon4@student00.cse.nd.edu:~/SP/homework04/reddit
import os
import requests
import re
import sys

#defualt values
FIELD = 'title'
FP = 0 #if field is chosen
LIMIT = 10
SUBREDDIT = 'linux'
URL = 'https://www.reddit.com/r/'

def usage(status=0):
    print '''
    Usage: reddit.py [ -f FIELD -s SUBREDDIT ] regex
    -f FIELD        Which field to search (default: title)
    -n LIMIT        Limit number of articles to report (default: 10)
    -s SUBREDDIT    Which subreddit to search (default: linux)'''

    sys.exit(status)

args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    # TODO: Parse command line arguments
    if arg == '-f':
        FIELD= args.pop(0)
        FP = 1
    elif arg == '-n':
        LIMIT = int(args.pop(0))
    elif arg == '-s':
        SUBREDDIT = args.pop(0)
    elif arg == '-h':
        usage(0)
    else:
        usage(1)

if(len(args)):
    equal = re.compile(args.pop(0)) #takes care of reg expression



headers = {'user-agent': 'reddit-{}'.format(os.environ['USER'])}
response = requests.get(URL + SUBREDDIT + '.json' , headers=headers)

REDtxt = response.json() #dictionary
if FIELD == 'title' or FIELD == 'author' or FIELD == 'link' or FIELD == 'short': #must be one of these fields in order to run
    if response.status_code == 200:
        for index,article in enumerate(REDtxt['data']['children']):
            if (index < LIMIT):
                if (FP == 1):
                    if(bool(equal.search(article.get('data').get(FIELD)))):
                        print '{}\tTitle:\t{}'.format(index+1, article.get('data').get('title'))
                        print '\tAuthor:\t{}'.format(article.get('data').get('author'))
                        print '\tLink:\t{}'.format('http://reddit.com{}'.format(article.get('data').get('permalink')))
                        shorturl = 'https://is.gd/create.php?format=simple&url={}'.format('https://reddit.com{}'.format(article.get('data').get('permalink')))
                        response = requests.get(shorturl, headers=headers)
                        print '\tShort:\t{}'.format(response.text)
                else:
                    print '{}\tTitle:\t{}'.format(index+1, article.get('data').get('title'))
                    print '\tAuthor:\t{}'.format(article.get('data').get('author'))
                    print '\tLink:\t{}'.format('http://reddit.com{}'.format(article.get('data').get('permalink')))
                    shorturl = 'https://is.gd/create.php?format=simple&url={}'.format('https://reddit.com{}'.format(article.get('data').get('permalink')))
                    response = requests.get(shorturl, headers=headers)
                    print '\tShort:\t{}'.format(response.text)
    else:
        print 'Invalid URL'
else:
    print 'Invalid Field'
    usage(2)
