Homework 04
===========

Activity 1:

How you parsed command line arguments.
To parse in the command line arguments, I used a while loop that categoried
each argument passed with its corresponding flag. If an argument could be
followed by a number such as the delay or the stepsize, args.pop(0) was used. This is similar to shift in unix shell scripting. It was turned into an integer if the argument passed was a number.

How you managed the temporary workspace.
To manage it, I opened a temporary workspace. I placed all of my temporary gif files onto it and deleted them once the final gif was made. II deleted this using the removeTemp function.

How you extracted the portrait URLS.
First used the request.get() to get the URL of the image. Then the content of this URL was then searched and basically grepped using the re.findall() function. This uses a reg expression <img src="(.*profiles[^"]*) to return the contents of the URL+netid.jpg and places this in a variable called PortraitURL. We have extracted the URLs.  

How you downloaded the portrait images.
To download the URLs we used request.get(the first element in PortraitURL). This places the content of the URL into the variable jpg1 and jpg2. In order to place them onto our workspace, we used the os.path.join command. We downloaded and wrote the image files using with open(wherever we want to store file, write) as f:
f.write(the content of the Urls).

How you generated the blended composite images.
I used the composite command to make many different images with a different blend percentages. I used a for loop that iterated based on STEPSIZE in order to make these images. The range was from 0 to 101. This created images and placed them onto the temporary workspace.  

How you generated the final animation.
To create the final animation I used the convert command followed by a string containing the names of all the gif pictures previously created. To make this string, for loops were used which simply iterated through the names of the pictures and added to the string variable. in order to get the final gif, + 'final.gif' was added to the string when the command was executed using os.system.

How you checked for failure of different operations and exited early.
to check for failure, I checked the status_code of both request url commands and if the value of it was not 200 (ie not successful), then the program displayed an error message and exited early.

Activity 2:

How you parsed command line arguments.
To parse in command line arguments, I used a while loop that ran while there were arguments. Depending on the flag, I used args.pop(0) to pass in the next argument and set it as one of the global variables. The if statements took care of each different flag. in order to compile a regular expression pattern into an object which can be used for matching, I used re.compile.

How you fetched the JSON data and iterated over the articles.
To fetch the JSON data, I first requested it using request.get and then placed the data on the variable REDtxt by using the command
REDtxt = response.json(). To iterate over the articles, I used a for loop that ran through REDtxt's ['data']['children']. As long as the index was less than the specified limit, and there was a valid field, it would continue.

How you filtered each article based on the FIELD and REGEX.
To do this, I used the if statement:
if(bool(equal.search(article.get('data').get(FIELD))))
The "equal" takes care of the regular expression while the field is filtered based on the value of .get(FIELD) within this line.

How you generated the shortened URL.
To generate the short url, I used the website https://is.gd/create.php?format=simple&url=
which generates a short url. I used a {} and a .format to make it into a url format. We used the websites data and permalink. request.get into a response variable returned the actual text.
