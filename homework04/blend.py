#!/usr/bin/env python2.7
#scp ./* jleon4@student00.cse.nd.edu:~/SP/homework04 to import

import atexit
import os
import re
import shutil
import sys
import tempfile
import requests

# Global variables

REVERSE     = False
DELAY       = 20
STEPSIZE    = 5
URL = 'https://engineering.nd.edu/profiles/'


# Functions

def usage(status=0):
    print '''Usage: {} [ -r -d DELAY -s STEPSIZE ] netid1 netid2 target
    -r          Blend forward and backward
    -d DELAY    GIF delay between frames (default: {})
    -s STEPSIZE Blending percentage increment (default: {})'''.format(
        os.path.basename(sys.argv[0]), DELAY, STEPSIZE
    )
    sys.exit(status)

# Parse command line options

args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    # TODO: Parse command line arguments
    if arg == '-r':
        REVERSE = True
    elif arg == '-d':
        arg = (args.pop(0))
        DELAY = int(arg)
    elif arg == '-s':
        arg = (args.pop(0))
        STEPSIZE = int(arg)
    else:
        usage(1)

if len(args) != 3:
    usage(1)

netid1 = args[0]
netid2 = args[1]
target = args[2]

# Main execution

# TODO: Create workspace
WORKSPACE = tempfile.mkdtemp(prefix = 'blend')

# TODO: Register cleanup

def removeTemp():
        shutil.rmtree(WORKSPACE)
        sys.exit(0)

atexit.register(removeTemp)

# TODO: Extract portrait URLs
RequestURL1 = requests.get(URL+netid1)
RequestURL2 = requests.get(URL+netid2)
PortraitURL1 = re.findall('<img src="(.*profiles[^"]*)', RequestURL1.content)
PortraitURL2 = re.findall('<img src="(.*profiles[^"]*)', RequestURL2.content)

if RequestURL1.status_code != 200 or RequestURL2.status_code != 200:
    print 'Invalid URL or Link'
    sys.exit(1)

# TODO: Download portraits

jpg1 = requests.get(PortraitURL1[0])
jpg2 = requests.get(PortraitURL2[0])

path1 = os.path.join(WORKSPACE,'image1.jpg')
path2 = os.path.join(WORKSPACE,'image2.jpg')

with open(path1,'w') as f:
    f.write(jpg1.content)
with open(path2,'w') as f:
    f.write(jpg2.content)


# TODO: Generate blended composite images
for stepsize in range(0,101,STEPSIZE):
    command ='composite -blend {} {} {} {}/out{}.gif'.format(stepsize,path1,path2,WORKSPACE,stepsize)
    os.system(command)


# TODO: Generate final animation

if REVERSE == True:
    string = 'convert -loop 0 -delay {} '.format(DELAY)
    for stepsize in range(0,101,STEPSIZE):
        string += '{}/out{}.gif{}'.format(WORKSPACE,stepsize,' ')
    for stepsize in range(100,0,-STEPSIZE):
        string += '{}/out{}.gif{}'.format(WORKSPACE,stepsize,' ')

    os.system(string + 'final.gif')
else:
    string = 'convert -loop 0 -delay {} '.format(DELAY)
    for stepsize in range(0,101,STEPSIZE):
        string += '{}/out{}.gif{}'.format(WORKSPACE,stepsize,' ')

    os.system(string + 'final.gif')
