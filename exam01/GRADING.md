GRADING - Exam 01
=================

- Commands:         2.25
- Short Answers:
    - Files:        2.5 
    - Processes:    2.75 
    - Pipelines:    2.25
- Debugging:        3.25
- Code Evaluation:  3 
- Filters:          2.75 
- Scripting:        2.75 
- Total:	    21.5
