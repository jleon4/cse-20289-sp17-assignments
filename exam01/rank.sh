#!/bin/sh

Number=5
Rank=0

sorting (){
  if [ $Rank -eq 1 ]; then
	sort -k1,1nr
  else
	sort -n
  fi
}

while [ $# -gt 0 ]
do
  case $1 in
  	-h)
    echo "Usage: rank.sh [-n N -D]"

    exit 1
  		;;
    -n)
      Number=$2
      shift
      ;;
  	-D)
      Rank=1
  		;;
  esac
  shift
done

echo | sorting | head -n $Number 
