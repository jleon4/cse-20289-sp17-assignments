#!/usr/bin/env python2.7

import sys
import os.path

if len(sys.argv)<2:
    print('No files to check')
    sys.exit(1)

else:
    for arg in sys.argv[1:]:
        if os.path.isfile(arg):
            print arg, 'does exits!'
        else:
            print arg, 'does not exist!'

    
