Reading 05
==========
1. What is the purpose of import sys?

the sys module is imported. This was we can use argv in order to get the list of commands passed onto the python script.

2. Explain what for arg in sys.argv[1:]: does.

This is significant for the first argument passed.

3. Why is there a trailing , in print arg,?

The main advantages are that it makes multi-line lists easier to edit and that it reduces clutter in diffs. Further, it prevents the new line character from being printed.


1. Explain how the command line arguments are parsed by discussing how this loop while len(args) and args[0].startswith('-') and len(args) > 1 works.

This while loop will continue as long as the arguments exists. It uses the array of args to check each file. The args.pop(0) iterates to the next argument/ file. It first checks if it is empty with lens(args). Next it checks for a flag using the startswith method. Next it checks if the argument followed by the flag is a command and not just the - character.

2. What are the purposes of the following code blocks:

The first code block identifies args as empty, it will then apprehend any "-" characters. If the argument is not blank, the second if statement will make the stream the standard input from the user. Otherwise, the program will open the designation of the user that was in the command line as stream. Essentially, this allows it to be read line by line in the next argument.

3. The method rstrip() returns a copy of the string in which all chars have been stripped from the end of the string (default whitespace characters). It is necessary for this situation.
