reading02 - Grading
===================

**Score**: 3/4

Deductions
----------
-0.25 6. $1 is the first positional argument
-0.25 8. The script tests if the first positional argument exists as a file
-0.25 You do not exit with an error code if one of the files does not exist
-0.25 When you check to see if no arguments were passed, you should use either
[ -e $1 ] or [ $# -gt 1 ]. You don't have a space before your last bracket and
the == does not work in this case.

Comments
--------
Good work!
