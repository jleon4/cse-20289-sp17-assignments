#!/bin/sh

if [ $1 == " "]; then
    echo "No arguments!"
    exit 1
fi

for TOKEN in $*
do
  if [ -e "$TOKEN" ]; then
      echo "$TOKEN exists!"
  else
      echo "$TOKEN does not exist!"
      exit 1
  fi
done

exit 0
