Reading 02
==========

How would you run the script even though it is not executable?
/bin/sh exists.sh

How would you make this script executable?
chmod 755 exists.sh would make it an executable

Once this script is executable, how would you run it directly?
./exists.sh

What is the purpose of the line #!/bin/sh?
The first line tells Unix that the file is to be executed by /bin/sh.

What is the output of the script if you run it with the argument * ?
exists.sh exists!

What is the $1 that appears in the script?
the first parameter in this case the name of the file

What does test -e "$1" do?
tests of the file exists

What does this script do?
This script tests if the file exists.

Write a new version of exists.sh with the following modifications:
#!/bin/sh

if [ $1 == " "]; then
    echo "No arguments!"
    exit 1
fi

for TOKEN in $*
do
  if [ -e "$TOKEN" ]; then
      echo "$TOKEN exists!"
  else
      echo "$TOKEN does not exist!"
  fi
done
