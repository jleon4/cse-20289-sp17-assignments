Reading 10
==========

Identify which system calls you would use to perform the following actions by briefly listing example C code:

1. Print out the error message associated with a failed system call.
perror()

#include <stdio.h>
     int main()
     {
        int i;
        extern int errno, sys_nerr;

        for (i = 0; i < sys_nerr; ++i)
           {
           fprintf(stderr, "%3d",i);
           errno = i;
           perror(" ");
           }
        exit (0);
     }

2. Truncate a file.
truncate()

#include <unistd.h>

 int ftruncate(int file_descriptor, off_t length);  

3. Move to the 10th byte of a file.

lseek(fd, 10, SEEK_SET);

4. Check if a path is a directory.

Use the S_ISDIR macro to check the mode bits in the stat struct:

struct stat s;
stat("/tmp", &s);
if (S_ISDIR(s.st_mode)) { ...

5. Create a copy of the current process.
fork()

printf("I'm printed once!\n");
fork();
// Now there are two processes running
// and each process will print out the next line.
printf("You see this line twice!\n");

6. Replace the code in the current process with another program.
exec()

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char**argv) {
  pid_t child = fork();
  if (child == -1) return EXIT_FAILURE;
  if (child) { /* I have a child! * /
    int status;
    waitpid(child , &status ,0);
    return EXIT_SUCCESS;

  } else { /* I am the child * /
    // Other versions of exec pass in arguments as arrays
    // Remember first arg is the program name
    // Last arg must be a char pointer to NULL

    execl("/bin/ls", "ls","-alh", (char * ) NULL);

    // If we get to this line, something went wrong!
    perror("exec failed!");
  }
}


7. Tell a process to terminate.
In C, send a signal to the child using kill POSIX call,
kill()

SIGINT	Terminate Process (Can be caught)	Tell the process to stop nicely
SIGQUIT	Terminate Process (Can be caught)	Tells the process to stop harshly
SIGKILL	Terminate Process (Cannot be Ignored)	You want your process gone

kill -s TERM 46

8. Receive the exit status of a child process.
Use waitpid (or wait).

pid_t child_id = fork();
if (child_id == -1) { perror("fork"); exit(EXIT_FAILURE);}
if (child_id > 0) {
  // We have a child! Get their exit code
  int status;
  waitpid( child_id, &status, 0 );
