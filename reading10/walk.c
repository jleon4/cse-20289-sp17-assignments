//Abraham Leon

#include <sys/stat.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
  struct dirent *pDirent;
  DIR *pDir = opendir(".");
  while((pDirent= readdir(pDir))){
    if (pDirent->d_type == DT_REG){
      FILE *file = fopen(pDirent->d_name, "r");
      int filedescriptor = fileno(file);
      struct stat buff;
      fstat(filedescriptor, & buff);
      printf("%s %d\n", pDirent->d_name, (int)buff.st_size);
      fclose(file);
    }
  }
  closedir(pDir);
  return 0;


}
