/* freq.c */

#include "map.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
    fprintf(stderr, "    -f FORMAT        Output format (KEY, KEY_VALUE, VALUE, VALUE_KEY)\n");
    fprintf(stderr, "    -l LOAD_FACTOR   Load factor for hash table\n");
    exit(status);
}

void freq_stream(FILE *stream, double load_factor, DumpMode mode) {
  char buffer[BUFSIZ];
  Map* HashTable = map_create(0, load_factor);

  // while (fscanf(stdin, "%s", buffer) != EOF){
  //   //printf("The word is: %s\n", buffer;);
  //   Entry * searched = map_search(HashTable, buffer);
  //   if (searched == NULL){
  //     Value value;
  //     value.number = 1;
  //     map_insert(HashTable, searched->key, value, NUMBER);
  //   }
  //   else {
  //     map_insert(HashTable, searched->key, (Value)(searched->value.number +1), NUMBER);
  //   }
  // }

  while (fgets(buffer, BUFSIZ, stream)){
    char * word = strtok(buffer, "\t\n "); //splits input into separate words
    while (word != NULL) {
      Entry * searched = map_search(HashTable, buffer);
      if (searched == NULL){
        Value value;
        value.number = 1;
        map_insert(HashTable, word, value, NUMBER);
      }
      else {
        map_insert(HashTable, word, (Value)(searched->value.number +1), NUMBER);
      }
      word = strtok(NULL, " \t\n");
    }
  }


  map_dump(HashTable, stdout, mode);
  map_delete(HashTable);

}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* Parse command line arguments */

    int argind = 1;
    DumpMode mode = VALUE_KEY;

    double LF = 0;

    PROGRAM_NAME = argv[0];
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        switch (arg[1]) {
            case 'h':
                usage(0);
                break;
            case 'f':
                if (strcmp(argv[argind], "KEY")==0){mode = KEY;}
                else if (strcmp(argv[argind], "KEY_VALUE")==0){mode = KEY_VALUE;}
                else if (strcmp(argv[argind], "VALUE")==0){mode = VALUE;}
                else if (strcmp(argv[argind], "VALUE_KEY")==0){mode = VALUE_KEY;}

                break;
            case 'l':
                LF = atof(argv[argind++]);
                break;
            default:
                usage(1);
                break;
        }
    }



     /* Compute frequencies of data from stdin */
    freq_stream(stdin, LF, mode);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
