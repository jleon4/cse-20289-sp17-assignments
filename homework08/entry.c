/* entry.c: map entry */
//scp ./* jleon4@student00.cse.nd.edu:~/SP/homework08/
#include "map.h"

/**
 * Create entry structure.
 * @param   key             Entry's key.
 * @param   value           Entry's value.
 * @param   next            Reference to next entry.
 * @param   type            Entry's value's type.
 * @return  Allocate Entry structure.
 */
Entry *		entry_create(const char *key, const Value value, Entry *next, Type type) {
  Entry *create = malloc(sizeof(Entry));
  const char *k = strdup(key);
  create->key = k;
  if (type == STRING) { create->value.string = strdup(value.string);}
  else{
    create->value.number = value.number;
  }
  create->type = type;
  create->next = next;

  return create;
}

/**
 * Delete entry structure.
 * @param   e               Entry structure.
 * @param   recursive       Whether or not to delete remainder of entries.
 * return   NULL.
 */
Entry *		entry_delete(Entry *e, bool recursive) {
  if (recursive){
    while (e != NULL){
      free(e->key);
      if (e->type == STRING){free(e->value.string);}
      Entry *temp = e->next;
      free(e);
      e = temp;
    }
    free(e);
  }
  else{
    free(e->key);
    if (e->type == STRING){free(e->value.string);}
    free(e);
  }
  return NULL;
}

/**
 * Update entry's value.
 * @param   e               Entry structure.
 * @param   value           New value for entry.
 * @param   type            New value's type.
 */
void  entry_update(Entry *e, const Value value, Type type) {
  if (e->type == STRING){
    free(e->value.string);
  }
  if (type == STRING){
    e->value.string = strdup(value.string);
  }
  else{
    e->value = value;
  }
  e->type = type;
}

/**
 * Dump entry.
 * @param   e               Entry structure.
 * @param   stream          File stream to write to.
 * @param   mode            Dump mode to use.
 */
void            entry_dump(Entry *e, FILE *stream, const DumpMode mode) {
  if (e == NULL) {
        return;
    }

  if (e->type == STRING){
    switch (mode) {
          case KEY:  fprintf(stream,"%s\n",e->key); break;
          case KEY_VALUE: fprintf(stream,"%s\t,%s\n",e->key,e->value.string); break;
          case VALUE:  fprintf(stream,"%s\n",e->value.string); break;
          case VALUE_KEY:   fprintf(stream,"%s\t,%s\n",e->value.string,e->key); break;
      }
  }
  else{
    switch (mode) {
          case KEY:  fprintf(stream,"%s\n",e->key); break;
          case KEY_VALUE: fprintf(stream,"%s\t,%ld\n",e->key,e->value.number); break;
          case VALUE:  fprintf(stream,"%ld\n",e->value.number); break;
          case VALUE_KEY:   fprintf(stream,"%ld\t,%s\n",e->value.number,e->key); break;
      }
  }
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
