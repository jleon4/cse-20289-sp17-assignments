Homework 08
===========

1. Briefly describe what memory you allocated in entry_create and what memory you deallocated in entry_delete. How did you handle the recursive flag?

In entry create we allocated the memory necessary to allocate the struct Entry. We used did this by using Malloc. Next, we allocated for the key of this entry by using string duplicate. Using pointers, we set the value of key. If the value of type is string, we have to account for this by allocating memory using strdup, if is a number we can simply set value.number = value.number. We used pointers to set the value of type and next in our Entry.

In entry delete, we first need to see if the recursive option is true or false. If it is true, a while loop will recursively delete entries. As long as the entry is not NULL, we will free the key (as this is a char*), if the type is string, we need to free value.string as this is a char* and takes up an array of memory. If it is a number it will be deallocated when we free(e). We make a temporary Entry to be able to iterate through the entire bucket. If it is not recursive, we follow the same procedure without running through the entire bucket.

2. Briefly describe what memory you allocated in map_create and what memory you deallocated in map_delete. How did you handle internal entries?

IN map_create we allocated enough memory for the struct Map using malloc and the sizeof function to get the number of bytes in the Map struct. Next we set the capacity of this map by using pointers. If the capacity desire is 0, we set the capacity to the default capacity. , Same goes for the load factor. If the desired is 0, the load factor will be the set to the default. Since each bucket is composed of entries, we need to allocate the memory for this. Hence, we sue the calloc function order to allocate this memory. this effectively sets each sentinel’s next pointer to NULL. We set the size = 0. This map create was returned.

To deallocate this memory, We need to run through all of the buckets and recursively delete each buckets entry. Hence, we iterate through each bucket by using a for loop that runs from 0 to the max capacity of the map. Hence, this iterates through the map. At every bucket, we call the entry delete recursively. After iterating through all the buckets and clearing their entries, we free the buckets, and free m. We return null.


3. Briefly describe what memory you allocated, copied, and deallocated in map_resize. When is this function called and what exactly does it do to grow the map?

In map resize, we allocated memory to hold the new capacity aka the new number of buckets in our hash table. Next, we will copy the old buckets into the new buckets by using some temporary pointers. Our original buckets are saved into the original entry. the Entry pointer temp will point to the actual value of the entry(not the dummy node). We will iterate through each of these head values in each bucket, computing the new hashes and rehashing them onto the m->buckets. The next Entry pointer will be used to keep track of the next entry in our while loop. The capacity of the map is set to the new capacity.  

4. Briefly describe how your map_insert function works and analyze its time complexity and space complexity in terms of Big-O notation (both average and worst case).

My map insert function first checks to see if there are too many entries within our map. If does this by checking if the alpha is greater than the load factor set by the map create functions. If this condition is true, the map resize makes the map twice as big as before by setting its new capacity as twice the original. Next, we see if the entry with the given key exists within the map. We use the map search function to see this. If this function returns a NULL, it means that this entry was not found. Hence, we need to create a new entry. We use the hash function to compute the new hashes for this map. We will create the entry based on the given hash and insert the wanted value and type into this entry. We will increase the size of this bucket if this entry is inserted. If the found Entry exists, we will simply update this entry based on the Entry pointer found. The size of the bucket will not be increased.

Load Factor (alpha) = number of items / capacity.
When it come to separate chaining, on average, alpha is the number of things per bucket.
So the complexity for insert search and removal is gonna be O(alpha). Hence as long as we control or minimize alpha, we get constant time. To control alpha, we resize.

Time complexity:
- Average: Constant
- Worst: Constant as long as we control alpha

Space complexity:
- Average: Constant
- Worst: N

5. Briefly describe how your map_search function works and analyze its time complexity and space complexity in terms of Big-O notation (both average and worst case).

The map search function first calculates the hashes using the hash function. A temporary variable will point to the head of the Linked list. We will iterate temp through the Linked list to see if the the key exists. Using the string compare function, we will see if the key ecists within this. If the key exists, we will return the Entry pointer temp. If not, we will keep iterating through the loop until we reach the NULL variable.

Time complexity:
- Average: Constant
- Worst: Constant as long as we control alpha

Space complexity:
- Average: Constant
- Worst: N


6. Briefly describe how your map_remove function works and analyze its time complexity and space complexity in terms of Big-O notation (both average and worst case).

The map remove function works by first seeing if the key is found. This is done using the map search function which will return the Entry pointer found. If this is NULL, the entry is not found and we do not need to remove anything. False is returned. If it is found, we first compute the hashes for the given key. Set a temp variable to point to the head of he list. Using string compare we will see if the Entry's key matches the key that we want to remove. If it matches, we will call the entry delete function on this entry. We will then decrease the size of the bucket. The next else statement takes care of any possible key matches within the linked list, not just the first entry. It does a similar process as above.


1. Based on your experiments, what is the overall trend in terms of time and space as the number of items increases for the different load factors?

As the number of items increases, More space is required, More time is required as well. By manipulating the load factor, we can either increase/decrease the memory required or increase/decrease the time required. At low numbers, a higher load factor doesn't seem to benefit the time as much as with higher numbers. At low numbers, a lower load factor seems to be more efficient as it is a good balance. Increasing the load factor at a higher number will significantly decrease the memory, with some drawbacks in time. A good load factor has to be found depending on the size.

2. There is no such thing as a perfect data structure. Compare this hash table to the treap you are creating in your data structures class. What are the advantages and disadvantages of both? If you had to use one as the underlying data structure for a map, which one would you choose and why?

We never over allocate in a treap. However in a hash table, we can over allocate memory. We might prefer a treap over a hash table since a treap is sorted and a hash table is not. If we have to resize, you have to allocate and rehash everything. However a treap can be more consistant even though it is log(n) and insert is O(1) in hash table. On average, given a decent implementation we should get O(1) performance with a hash table. I would choose hash table to make a map. As we can get information much more quickly.
