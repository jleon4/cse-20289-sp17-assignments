Homework08 - Grading
====================

**Score**: 11.5/15

Deductions
----------
-1 test-map: fails some tests
-1 test_freq.sh: mostly fails
-.75 benchmark.{sh,py} and BENCH.md: missing
-.25 readme Act1 Q4: time complexity worst case is O(n)
-.25 readme Act1 Q5: time complexity worst case is O(n) and space complexity worst case is O(1)
-.25 readme Act1 Q6: time complexity O(n) in worst case and O(1) in average case while space complexity is O(1) in average and worst case

Comments
--------
