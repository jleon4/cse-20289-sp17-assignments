/* map.c: separate chaining hash table */

#include "map.h"

/**
 * Create map data structure.
 * @param   capacity        Number of buckets in the hash table.
 * @param   load_factor     Maximum load factor before resizing.
 * @return  Allocated Map structure.
 */
Map *	 map_create(size_t capacity, double load_factor) {
  Map *create = malloc(sizeof(Map)); //allocates new map
  create->capacity = capacity;
  if (capacity == 0){
    create->capacity = DEFAULT_CAPACITY;
  }
  create->load_factor = load_factor;
  if (load_factor == 0){
    create->load_factor = DEFAULT_LOAD_FACTOR;
  }
  create->buckets = calloc(create->capacity, sizeof(Entry));
  create->size = 0;

  return create;
}

/**
 * Delete map data structure.
 * @param   m               Map data structure.
 * @return  NULL.
 */
Map *	map_delete(Map *m) {
  // free(m->buckets);
  // free(m);
  //   return NULL;
  if (m->size == 0){
    return NULL;
  }
  for (int i = 0; i < m->capacity; i++){
    Entry *next = m->buckets[i].next; //points to head
    entry_delete(next, true);
  }
  free(m->buckets);
  free(m);


  return NULL;
}

/**
 * Insert or update entry into map data structure.
 * @param   m               Map data structure.
 * @param   key             Entry's key.
 * @param   value           Entry's value.
 * @param   type            Entry's value's type.
 */
void  map_insert(Map *m, const char *key, const Value value, Type type) {

  //checks if its too big
  if (m->load_factor < (double)m->size/m->capacity){
    map_resize(m,2*((double)m->capacity));
  }

  Entry * found = map_search(m, key);
  //calculate hash for table
  uint64_t hash = fnv_hash(key, strlen(key));
  hash = hash%m->capacity; //computes new hash
  Entry *next = m->buckets[hash].next; //points to head
  //if not found, insert

  if (found == NULL){
    m->buckets[hash].next = entry_create(key,value, next, type);
    m->size++;
  }
  //else if it does exist, update value
  else{
    entry_update(found, value, type);
  }
}

/**
 * Search map data structure by key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to search for.
 * @param   Pointer to the entry with the specified key (or NULL if not found).
 */
Entry *  map_search(Map *m, const char *key) {

  uint64_t hash = fnv_hash(key, strlen(key));
  hash = hash%m->capacity;
  Entry *temp = m->buckets[hash].next; //points to head
  while (temp != NULL){
    if (strcmp(temp->key, key)==0){
      return temp;
    }
    temp = temp->next;
  }
  //printf("Not found\n");
  return NULL;
}

/**
 * Remove entry from map data structure with specified key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to remove.
 * return   Whether or not the removal was successful.
 */
bool  map_remove(Map *m, const char *key) {

  Entry * found = map_search(m, key);
  if (found == NULL){
    return false;
  }
  else{
    uint64_t hash = fnv_hash(key, strlen(key));
    hash = hash%m->capacity; //computes hash
    Entry * temp = m->buckets[hash].next; //points to the actual entry, not
    if (strcmp(temp->key, key)==0){
      //Entry * tobedeleted = temp;
      m->buckets[hash].next = temp->next;
      entry_delete(temp, false);
      //temp->next = tobedeleted->next;
      m->size--;
      return true;
    }
    else{
      while(strcmp(temp->next->key,key) != 0 && temp != NULL){
        temp = temp->next;
      }
      if (temp != NULL){
        Entry * tobedeleted = temp->next;
        temp->next = tobedeleted->next;
        entry_delete(tobedeleted, false);
        m->size--;
        return true;
      }
    }
  }
}




/**
 * Dump all the entries in the map data structure.
 * @param   m               Map data structure.
 * @param   stream          File stream to write to.
 * @param   mode            Dump mode to use.
 */
void map_dump(Map *m, FILE *stream, const DumpMode mode) {
  if (m == NULL) {
        return;
    }
  for (int i = 0; i < m->capacity; i++){
    Entry * temp = m->buckets[i].next;
    while (temp != NULL){
      entry_dump(temp, stream, mode);
      temp = temp->next;
    }
  }
}

/**
 * Resize the map data structure.
 * @param   m               Map data structure.
 * @param   new_capacity    New capacity for the map data structure.
 */
void map_resize(Map *m, size_t new_capacity) {

  Entry * original = m->buckets;

  m->buckets = calloc(new_capacity, sizeof(Entry));

  for (int i = 0; i < m->capacity; i++){
    Entry * next = NULL; //keep track of next entry in while loop
    Entry * temp = original[i].next; //points to the head
    while (temp != NULL){
      uint64_t hash = fnv_hash(temp->key, strlen(temp->key));
      hash = hash%new_capacity; //computes new hash
      next = temp->next;
      temp->next = m->buckets[hash].next; //points temp->next to NULL technically

      m->buckets[hash].next = temp;

      temp = next;

    }
  }
  m->capacity = new_capacity;

  free(original);


  // Map *newMap = malloc(sizeof(Map)); //allocates new map
  // newMap->capacity = new_capacity;
  // if (new_capacity == 0){
  //   newMap->capacity = DEFAULT_CAPACITY;
  // }
  // newMap->load_factor = m->load_factor;
  // newMap->buckets = calloc(newMap->capacity, sizeof(Entry));
  //
  // //re hash entries in list with new capacity
  // uint64_t hash = fnv_hash(newMap->buckets->key, strlen(newMap->buckets->key)); //whos key?
  // hash = hash%newMap->capacity;
  // Entry *temp = m->buckets[hash].next;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
