Reading 01
==========

1a) This is a pipe operator, this connects the output of the first command to the input of the second command.
b) 2 refers to the standard error. This is redirecting the standard error to the bit bucket. This suppresses errors messages from a command.
c) This redirection operator connects the with the file output.txt.
d) This displays file sizes in human readable format rather than in bytes.
e) These are the same commands. The order of the redirection of the standard error does not matter in this case.

2a) cat 2002-{1..12} > newfile.txt
b) cat {2002..2006}-12 > newfile.txt
c) cat {2002..2006}-{01..06}
d) cat {2002,2004,2006}-{01,03,05,07,09,11}
e) cat {2002..2004}-{09..12}

3a) files 2 and 3 are executable by owner
b) files 2 and 3 are readable by members of the users group
c) None
d) chmod 755 filename.txt
e) chmod 600 filename.txt

4a) bg bc or Control + Z. 
b) fg bc
c) Control + D
d) halt
e) sudo bg bc
