/* lsort.c */
//abraham leon

#include "list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
    fprintf(stderr, "  -n   Numerical sort\n");
    fprintf(stderr, "  -q   Quick sort\n");
    exit(status);
}

void lsort(FILE *stream, bool numeric, bool quicksort) {

  //ask for memory
  struct list * theList = list_create();

  char buffer[BUFSIZ];
  while (fgets(buffer,BUFSIZ,stream)){
    list_push_back(theList, buffer);
    //numeric
    if (numeric == true){
      if (quicksort == true){
        list_qsort(theList, node_compare_number);
      }
      else {
        list_msort(theList, node_compare_number);
      }
    }
    //not numeric
    else{
      if (quicksort == true){
        list_qsort(theList, node_compare_string);
      }
      else {
        list_msort(theList, node_compare_string);
      }
    }
  }

  for (struct node *curr = theList->head; curr != NULL; curr = curr->next) {
    printf("%s",curr->string );
    }

    //free the memory
  list_delete(theList);

}

/* Main Execution */

int main(int argc, char *argv[]) {

    /* Parse command line arguments */
    int argind = 1;

    bool numeric = false;
    bool quicksort = false;

    PROGRAM_NAME = argv[0];
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        switch (arg[1]) {
            case 'h':
                usage(0);
                break;
            case 'n':
                numeric = true;
                break;
            case 'q':
                quicksort = true;
                break;
            default:
                usage(1);
                break;
        }
    }

    /* Sort using list */
    lsort(stdin, numeric, quicksort);

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
