/* list.c */

#include "list.h"

/* Internal Function Prototypes */

static struct node*	reverse(struct node *curr, struct node *next);
static struct node *	msort(struct node *head, node_compare f);
static void		split(struct node *head, struct node **left, struct node **right);
static struct node *	merge(struct node *left, struct node *right, node_compare f);

/**
 * Create list.
 *
 * This allocates a list that the user must later deallocate.
 * @return  Allocated list structure.
 */
struct list *	list_create() {
  struct list *create = malloc(sizeof(struct list)); //allocated memory
  create->head = NULL;
  create->tail = NULL;
  create->size = 0;

  return create;
  return NULL;
}

/**
 * Delete list.
 *
 * This deallocates the given list along with the nodes inside the list.
 * @param   l	    List to deallocate.
 * @return  NULL pointer.
 */
struct list *	list_delete(struct list *l) {
  if (l->size == 0){
    return NULL;
  }
  node_delete(l->head, true);
  // else{
  //   while(temp->head != NULL){
  //     struct node *temp2 = n->next;
  //     node_delete(temp->head, false);
  //   }
  // }
  free(l);
  return NULL;
}

/**
 * Push front.
 *
 * This adds a new node containing the given string to the front of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void		list_push_front(struct list *l, char *s) {
  struct node *newNode = node_create(s,l->head);
  l->head = newNode;
  if (l->tail == NULL){
    l->tail = l->head;
  }
  (l->size)++;
}

/**
 * Push back.
 *
 * This adds a new node containing the given string to the back of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void		list_push_back(struct list *l, char *s) {
  (l->size)++;
  struct node *temp = node_create(s, NULL);
    if (l->size > 1){
      l->tail->next = temp;
      l->tail = temp;
    }
    else{
      l->tail = temp;
      l->head = temp;
    }

  //l->size = l->size + 1;
  // struct node *newNode = node_create(s,l->tail);
  // l->tail = newNode;
  // if (l->tail == NULL){
  //   l->tail = l->head;
  // }
  // (l->size)++;
}

/**
 * Dump list to stream.
 *
 * This dumps out all the nodes in the list to the given stream.
 * @param   l	    List structure.
 * @param   stream  File stream.
 */
void		list_dump(struct list *l, FILE *stream) {
  for (struct node *curr = l->head; curr != NULL; curr = curr->next) {
	   node_dump(curr, stream);
    }
}

/**
 * Convert list to array.
 *
 * This copies the pointers to nodes in the list to a newly allocate array that
 * the user must later deallocate.
 * @param   l	    List structure.
 * @return  Allocate array of pointers to nodes.
 */
struct node **	list_to_array(struct list *l) {
  int size = l->size;
  struct node * temp = l->head;
  struct node **array = malloc(sizeof(struct node*)*size); //allocated memory
  for (int i = 0; i < size; i++){
    array[i] = temp;
    temp = temp->next;
  }
    return array;
}

/**
 * Sort list using qsort.
 *
 * This sorts the list using the qsort function from the standard C library.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void		list_qsort(struct list *l, node_compare f) {
  int length = l->size;
  struct node ** theArray = list_to_array(l); //converts list to array
  qsort(theArray, length, sizeof(struct node *), f);
  l->head = theArray[0];
  l->tail = theArray[length-1];
  for (int i = 0; i < length-1; i++){
    theArray[i]->next = theArray[i+1];
  }
  l->tail->next = NULL;

  free(theArray);
}

/**
 * Reverse list.
 *
 * This reverses the list.
 * @param   l	    List structure.
 */
void		list_reverse(struct list *l) {
  l->tail = l->head;
  l->head = reverse(l->head, NULL);

}

/**
 * Reverse node.
 *
 * This internal function recursively reverses the node.
 * @param   curr    The current node.
 * @param   prev    The previous node.
 * @return  The new head of the singly-linked list.
 */
struct node*	reverse(struct node *curr, struct node *prev) {
  struct node *tail = curr;
  if(curr->next){
    tail = reverse(curr->next, curr);
  }
  curr->next = prev;
  return tail;
}

/**
 * Sort list using merge sort.
 *
 * This sorts the list using a custom implementation of merge sort.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void		list_msort(struct list *l, node_compare f) {
  l->head = msort(l->head, f);
  struct node * temp = l->head;
  while(temp->next != NULL){
    temp = temp->next;
  }
  l->tail = temp;
  l->tail->next = NULL;

}

/**
 * Performs recursive merge sort.
 *
 * This internal function performs a recursive merge sort on the singly-linked
 * list starting with head.
 * @param   head    The first node in a singly-linked list.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node *	msort(struct node *head, node_compare f) {

  if ((head == NULL) || (head->next == NULL)){
    return head;
  }
  struct node *left;
  struct node *right;

  split(head,&left,&right);

  left  = msort(left, f);
  right = msort(right, f);
  head = merge(left, right, f);
  return head;

}

/**
 * Splits the list.
 *
 * This internal function splits the singly-linked list starting with head into
 * left and right sublists.
 * @param   head    The first node in a singly-linked list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 */
void	split(struct node *head, struct node **left, struct node **right) {
  struct node * fast = head->next->next;
  struct node * slow = head->next;
  struct node * tail = head;
  while (fast != NULL && fast->next != NULL){
        fast = fast->next->next;
        tail = slow;
        slow = slow->next;
  }

  if (tail != NULL){
    tail->next = NULL;
  }
  //sets new pointers
  *right = slow;
  *left = head;

}

/**
 * Merge sublists.
 *
 * This internal function merges the left and right sublists into one ordered
 * list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node *	merge(struct node *left, struct node *right, node_compare f) {
  struct node * lpointer = left;
  struct node * rpointer = right;
  struct node * iter = left;

  if (f(&lpointer, &rpointer)<0){
    iter = lpointer;
    lpointer = lpointer->next;
  }
  else {
    iter = rpointer;
    rpointer = rpointer->next;
  }

  struct node * newHead = iter;

  while(lpointer&&rpointer){
    if (f(&lpointer, &rpointer)<0){
      iter->next = lpointer;
      lpointer = lpointer->next;
      iter = iter->next;
    }
    else {
      iter->next = rpointer;
      rpointer = rpointer->next;
      iter = iter->next;
    }
  }

  if (lpointer == NULL){iter->next = rpointer;}
  if (rpointer == NULL){iter->next = lpointer;}

  return newHead;
}
