Homework 07
===========

Abraham Leon

1. Describe what memory you allocated in node_create and what memory you deallocated in node_delete. How did you handle the recursive flag?

In the node create, we allocated enough memory to contain the struct node. Hence, we "asked" for char	* string, int number and struct node * next. To get the number of bytes required we simply used sizeof() function. Malloc allowed us to allocate the memory for this.
In node_delete, the string of the node is freed as this is an array that needs to be deallocated. It then checks if the value of recursive is true, if it is, a while loop that runs if thev value of the node is not NULL frees the string of node and the node itself. A temp node variable stores the value of the next node and is set to node during every iteration. If it is not called recursively, it simply frees string and n.


2. Describe what memory you allocated in list_create and what memory you deallocated in list_delete. How did you handle internal struct nodes?

I used malloc to allocate the size of a list, which contains a head, tail and a size. The struct nodes within a list are included with the sizeof function. In list_delete, I called upon the node delete recursively. This deleted all of the nodes and everything that they held inside all together. Further, I made sure to free(l), the actual list variable as this also took up space.


3. Describe how your list_qsort function works and analyze its time complexity and space complexity in terms of Big-O notation (both average and worst case).

This function works by converting the linked list to an array and using the built in qsort on this array to sort it. I made sure to set the head of the list as the first element in the array and the tail as the very last element in the array. In order to set the values of the array. A for loop iterates from 0 to the length of the linked list and sets the value of the next node in the list to the value of the array[i+1].

On average, qsort will be O (nlog(n)).
In the worst case, because of a pivot, quick sort can be O(n^2)
The space complexity will be O log(n)

4. Describe how your list_reverse function works and analyze its time complexity and space complexity in terms of Big-O notation (both average and worst case).

You need to have two functions. reverse which returns a struct node and list_reverse which calls upon reverse and actually reverses the list. The list_reverse function provides the user a more convenient function to call upon.
The reverse function will update the head and the tail of the given.
If pictured, this function is simply reassigning pointers recursively. Its assigning them to go the opposite way.
On average, reverse will be O (nlog(n)).
In the worst case, the time complexity for this function will be O (nlog(n))
The space complexity will be O logn

5. Describe how your list_msort function works and analyze its time complexity and space complexity in terms of Big-O notation (both average and worst case).

Divide and conquer.
The split function divides the list into left and right.
Next, merge is called recursively. The split function has a fast and slow node that will determine the new left and right nodes. We pass these values in by Reference into the split function. the msort function will call itself recursively, essentially splitting the linked list until there is only one element. It is basically creating a bunch of new linked lists. After it can no longer be split, the merge function is called. This function will relink the lists, but it will merge them in order. Using a left pointer a right pinter and an iterator, this merge will return the new head of the linked list. It is essentially just reassigning pointer values. To update the head and tail fields of the list, we use a temporary node within the list_msort function. It will find start from the original head and run iterate through the list until the very last one and set this as the new tail. 

In merge sort, the maximum levels I will have is O log(n). In theory, this can be represented as a tree. The height of this tree is O log(n). In each level of this tree, O log(n) of work is being done. Hence, the time complexity for merge sort is O (nlog(n)) as its average, best and its worst.
The space complexity for this is constant at each level. This is because we are simply changing pointers (in Linked List). The number of pointers never grow. However, the space complexity order for the merge sort list is O log(n).



1. Based on your experiments, what is the overall trend in terms of time and space as the number of items increases for both of your sorting methods?

Are the results surprising to you? Explain why or why not.
Quicksort was faster but used more memory. This is surprising. This is probably because we used the built in qsort function which expects an array. We basically duplicated the data. However an array is always continous. Unlike nodes, which can be anywhere in the heap. Hence, this is why the qsort was faster than the merge sort. It is simply a hardware advantage.  

Given what you said about the space complexity and time complexity of list_msort and list_qsort in Activity 1 and the experimental results in Activity 2, what can you say is the relationship between theoretical complexity and real world performance? Explain.

In terms of complexity, Quicksort has the worst complexity. Spacial locality has a lot to do with real world performance. Complexities cannot be the only things that we should rely on. Hardware should also be considered. Complexity is a measure of growth, it is not a measure of absolute performance, this will depend on our hardware.
