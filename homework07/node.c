/* node.c */
// scp ./* jleon4@student00.cse.nd.edu:~/SP/homework06/

#include "node.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/**
 * Create node.
 *
 * This allocates and configures a node that the user must later deallocate.
 * @param   string      String value.
 * @param   next        Reference to next node.
 * @return  Allocated node structure.
 */
struct node *	node_create(char *string, struct node *next) {
  struct node *create = malloc(sizeof(struct node)); //allocated memory
  char *s = strdup(string);
  create->string = s;
  create->number = atoi(s);
  create->next = next;

  return create;
  //free(create);
  //return NULL;
}

/**
 * Delete node.
 *
 * This deallocates the given node (recursively if specified).
 * @param   n           Node to deallocate.
 * @param   recursive   Whether or not to deallocate recursively.
 * @return  NULL pointer.
 */
struct node *   node_delete(struct node *n, bool recursive) {
  if (recursive){
    while (n != NULL){
      free(n->string);
      struct node *temp = n->next;
      free(n);
      n = temp;
    }
    free(n);
  }
  else{
    free(n->string);
    free(n);
  }
  return NULL;
}

/**
 * Dump node to stream.
 *
 * This dumps out the node structure (Node{string, number, next}) to the stream.
 * @param   n       Node structure.
 * @param   stream  File stream.
 **/
void node_dump(struct node *n, FILE *stream) {
  //modified to only print the string
  //fprintf(stream,"%s",n->string);

  fprintf(stream,"Node{%s,%i,%p}\n",n->string, n->number, n->next);
}

/**
 * Compare node structures by number
 *
 * This compares two node structures by their number values.
 * @param   a       First node structure.
 * @param   b       Second node structure.
 * @return  < 0 if a < b, 0 if a == 0, > 0 if a > b
 */
int		node_compare_number(const void *a, const void *b) {
  struct node *tempA = *((struct node**)a);
  struct node *tempB = *((struct node**)b);

  if(tempA->number < tempB->number){return -1;}
  if(tempA->number > tempB->number){return 1;}

  return 0;
}

/**
 * Compare node structures by string
 *
 * This compares two node structures by their string values.
 * @param   a       First node structure.
 * @param   b       Second node structure.
 * @return  < 0 if a < b, 0 if a == 0, > 0 if a > b
 */
int		node_compare_string(const void *a, const void *b) {
  struct node *tempA = *((struct node**)a);
  struct node *tempB = *((struct node**)b);

  return strcmp(tempA->string, tempB->string);

}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
